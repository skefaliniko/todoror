class Task < ApplicationRecord
  validates_presence_of :title
  validate :future_completed_date

  private

  def future_completed_date
    self.errors.add(:completed, "Back to the future?))") if !completed.blank? && completed < Date.today
  end
end
