class TasksController < ApplicationController
	def new
		@todo_task = Task.new
	end

	def create
		@todo_task = Task.create(task_params)
		@todo_tasks = Task.all
	end

	def edit
		@todo_task = Task.find(params[:id])
		render :new
	end

	def update
		@todo_task = Task.find(params[:id])
		@todo_task.update_attributes(task_params)
		@todo_tasks = Task.all
		render :create
	end

	def destroy
		@todo_task = Task.find(params[:id])
		@todo_task.destroy
		@todo_tasks = Task.all
	end

	private
	def task_params
		params.require(:task).permit(:title, :note, :completed)
	end

	private
	def apply
		if @todo_task.save
			@todo_tasks = Task.all
			render :create
		else
			render :new
		end
	end
end
